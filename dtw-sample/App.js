import React, { useState, useEffect } from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { Accelerometer } from 'expo-sensors';
let tiltGesture = require('./tilt_gesture.json');   //A sample gesture

export default function App() {
  //Create a currentPoint variable with default value [0,0,0] and a setCurrentPoint() function.
  // When we call setCurrentPoint stuff that uses currentPoint will get updated
  const [currentPoint, setCurrentPoint] = useState([0,0,0]);
  const [distance, setDistance] = useState(0);   //Same idea here
  const updates_per_second = 30;
  let data = [];
  let saveGesture = false;

  //Return the squared Euclidean distance; used by DTW
  const eucDistSquared = (p1, p2) => {
    return (p2.x - p1.x)**2 + (p2.y - p1.y)**2 + (p2.z - p1.z)**2;
  }


  //Start the accelerometer when the application starts
  useEffect(() => {
      _subscribe();
      Accelerometer.setUpdateInterval(updates_per_second);
  }, []);  //This causes a warning but ignore it. See https://reactjs.org/docs/hooks-effect.html#tip-optimizing-performance-by-skipping-effects


  //Returning a function means to call the function when the application exits
  useEffect(() => {
    return () => {
      _unsubscribe();
    };
  }, []);


  //Called for every point we get from the accelerometer
  const gotDataPoint = point => {
      setCurrentPoint(point);
      if(this.saveGesture) {
        this.data.push(point);
      }
  }


  //Subscribe to the accelerometer
  const _subscribe = () => {
    this._subscription = Accelerometer.addListener(accelerometerData => {
      gotDataPoint(accelerometerData)
    });
  };


  //Unsubscribe from the accelerometer
  const _unsubscribe = () => {
    this._subscription && this._subscription.remove();
    this._subscription = null;
  };


  //Gesture button is down; start saving gesture data
  const gestureButtonDown = () => {
    this.data = [];
    this.saveGesture = true;
  }


  //Gesture button released; stop saving gesture data and run DTW
  const gestureButtonUp = () => {
    this.saveGesture = false;
    console.log("Up!");
    let dtw = new DynamicTimeWarping(tiltGesture, this.data, eucDistSquared);
    setDistance(dtw.getDistance());
  }


  //Update UI stuff. When setCurrentPoint() gets called, currentPoint gets updated, so then
  // x,y,z get updated, so then the UI gets updated.
  let { x, y, z } = currentPoint;
  return (
    <View style={styles.sensor}>
      
      <Text style={styles.text}>Accelerometer: (in Gs where 1 G = 9.81 m s^-2)</Text>
      <Text style={styles.text}>
        x: {round(x)} y: {round(y)} z: {round(z)}
      </Text>

      <View style={styles.buttonContainer}>
        <TouchableOpacity style={styles.gestureButton}
        onPressIn={gestureButtonDown} onPressOut={gestureButtonUp}>
          <Text>Hold to gesture</Text>
        </TouchableOpacity>
      </View>

      <Text style={styles.text}>Result: </Text>
      <Text style={styles.text}>{round(distance)}</Text>

    </View>
  );
}

//Utility function
function round(n) {
  if (!n) {
    return 0;
  }
  return Math.floor(n * 100) / 100;
}

let styles = {
  buttonContainer: {
    flexDirection: 'row',
    alignItems: 'stretch',
    marginTop: 15,
  },
  button: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#eee',
    padding: 10,
  },
  middleButton: {
    borderLeftWidth: 1,
    borderRightWidth: 1,
    borderColor: '#ccc',
  },
  sensor: {
    marginTop: 45,
    paddingHorizontal: 10,
  },
  text: {
    textAlign: 'center',
  },
  gestureButton: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#ccf',
    width: '100%',
    height: 50,
  },
};


//Source: https://github.com/GordonLesti/dynamic-time-warping/issues/4
class DynamicTimeWarping {
    constructor(ts1, ts2, distanceFunction) {
        this.ser1 = ts1;
        this.ser2 = ts2;
        this.distFunc = distanceFunction;
        this.distance = null;
        this.matrix = null;
        this.path = null;
    }

    getDistance() {
        if (this.distance !== null) {
            return this.distance;
        }
        this.matrix = [];
        for (var i = 0; i < this.ser1.length; i++) {
            this.matrix[i] = [];
            for (var j = 0; j < this.ser2.length; j++) {
                var cost = Infinity;
                if (i > 0) {
                    cost = Math.min(cost, this.matrix[i - 1][j]);
                    if (j > 0) {
                        cost = Math.min(cost, this.matrix[i - 1][j - 1]);
                        cost = Math.min(cost, this.matrix[i][j - 1]);
                    }
                } else {
                    if (j > 0) {
                        cost = Math.min(cost, this.matrix[i][j - 1]);
                    } else {
                        cost = 0;
                    }
                }
                this.matrix[i][j] = cost + this.distFunc(this.ser1[i], this.ser2[j]);
            }
        }
        return this.matrix[this.ser1.length - 1][this.ser2.length - 1];
    }

    getPath() {
        if (this.path !== null) {
            return this.path;
        }
        if (this.matrix === null) {
            this.getDistance();
        }
        var i = this.ser1.length - 1;
        var j = this.ser2.length - 1;
        this.path = [[i, j]];
        while (i > 0 || j > 0) {
            if (i > 0) {
                if (j > 0) {
                    if (this.matrix[i - 1][j] < this.matrix[i - 1][j - 1]) {
                        if (this.matrix[i - 1][j] < this.matrix[i][j - 1]) {
                            this.path.push([i - 1, j]);
                            i--;
                        } else {
                            this.path.push([i, j - 1]);
                            j--;
                        }
                    } else {
                        if (this.matrix[i - 1][j - 1] < this.matrix[i][j - 1]) {
                            this.path.push([i - 1, j - 1]);
                            i--;
                            j--;
                        } else {
                            this.path.push([i, j - 1]);
                            j--;
                        }
                    }
                } else {
                    this.path.push([i - 1, j]);
                    i--;
                }
            } else {
                this.path.push([i, j - 1]);
                j--;
            }
        }
        this.path = this.path.reverse();
        return this.path;
    }
}

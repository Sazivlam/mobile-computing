# This is a sample Python script.

# Press Shift+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and conditions.
import pandas as pd
import math
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.pyplot import figure
pd.options.mode.chained_assignment = None  # default='warn'


def analyze(name):
    sitting = pd.read_csv(
        r'csvs/sitting.csv')
    walking = pd.read_csv(
        r'csvs/walking.csv')

    pd.set_option('display.max_rows', 2000)
    print(sitting[['position', 'condition', 'time', 'x', 'y', 'distance', 'width', 'miss', 'id']])

    #Sitting
    data = sitting[['condition', 'time', 'miss']]
    data['timeDiff'] = data[['time']].apply(lambda x: x.diff())
    misses = data.groupby('condition')['miss'].apply(lambda x: (x == True).sum()).reset_index(name='misses')
    data = data[['condition', 'timeDiff']].groupby(['condition']).mean()
    data = data.reset_index()
    final_results = data[['condition','timeDiff']]
    final_results['misses'] = misses.misses
    final_results['hitRate'] = (1- (final_results['misses'] / 20)) * 100
    print(final_results)
    walking_mean_difference = final_results['timeDiff'].mean()

    plt.scatter(final_results.condition, final_results.timeDiff)
    plt.axhline(walking_mean_difference, color='r', linestyle='--')
    plt.annotate('average (' + str('{:.4f}'.format(walking_mean_difference)) + ')',
                 xy=(3, walking_mean_difference),
                 xytext=(3, walking_mean_difference + 0.5),
                 )
    plt.title('Average time difference between clicks for each condition (sitting)')
    plt.xlabel('Condition')
    plt.ylabel('Average time difference between clicks (ms)')

    plt.show()
    plt.savefig('graphs/sitting_click_time.png', dpi=300)
    plt.cla()

    hitrate_mean = final_results['hitRate'].mean()
    plt.scatter(final_results.condition, final_results.hitRate)
    plt.axhline(hitrate_mean, color='r', linestyle='--')
    plt.annotate('average (' + str('{:.4f}'.format(hitrate_mean)) + ')',
                 xy=(3, hitrate_mean),
                 xytext=(3, hitrate_mean + 0.05),
                 )
    plt.title('Hit rate for each condition (sitting)')
    plt.xlabel('Condition')
    plt.ylabel('Hit rate (%)')

    plt.savefig('graphs/sitting_hit_rate.png', dpi=300)
    plt.show()
    plt.cla()

    # Walking
    data = walking[['condition', 'time', 'miss']]
    data['timeDiff'] = data[['time']].apply(lambda x: x.diff())
    misses = data.groupby('condition')['miss'].apply(lambda x: (x == True).sum()).reset_index(
        name='misses')
    data = data[['condition', 'timeDiff']].groupby(['condition']).mean()
    data = data.reset_index()
    final_results = data[['condition', 'timeDiff']]
    final_results['misses'] = misses.misses
    final_results['hitRate'] = (1 - (final_results['misses'] / 20)) * 100
    print(final_results)
    walking_mean_difference = final_results['timeDiff'].mean()

    plt.scatter(final_results.condition, final_results.timeDiff)
    plt.axhline(walking_mean_difference, color='r', linestyle='--')
    plt.annotate('average (' + str('{:.4f}'.format(walking_mean_difference)) + ')',
                 xy=(3, walking_mean_difference),
                 xytext=(3, walking_mean_difference + 0.5),
                 )
    plt.title('Average time difference between clicks for each condition (walking)')
    plt.xlabel('Condition')
    plt.ylabel('Average time difference between clicks (ms)')

    plt.savefig('graphs/walking_click_time.png', dpi=300)
    plt.show()
    plt.cla()

    hitrate_mean = final_results['hitRate'].mean()
    plt.scatter(final_results.condition, final_results.hitRate)
    plt.axhline(hitrate_mean, color='r', linestyle='--')
    plt.annotate('average (' + str('{:.4f}'.format(hitrate_mean)) + ')',
                 xy=(3, hitrate_mean),
                 xytext=(3, hitrate_mean + 0.05),
                 )
    plt.title('Hit rate for each condition (walking)')
    plt.xlabel('Condition')
    plt.ylabel('Hit rate (%)')

    plt.savefig('graphs/walking_hit_rate.png', dpi=300)
    plt.show()
    plt.cla()


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    analyze('PyCharm')

# See PyCharm help at https://www.jetbrains.com/help/pycharm/

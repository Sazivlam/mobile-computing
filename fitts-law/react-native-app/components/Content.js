import React, { useState } from 'react';
import { View, Button, Pressable, Share } from 'react-native';

const Content = props => {
	//Keep data to export
	const [data, setData] = useState([])

	const position = "sitting"
	const leftMargin = 70
	const requiredClicks = 20
	const baseDistance = 256
	const widths = [1, 2, 4, 8, 16, 32, 64]
	const distances = [
		baseDistance - widths[0],
		baseDistance - widths[1],
		baseDistance - widths[2],
		baseDistance - widths[3],
		baseDistance - widths[4],
		baseDistance - widths[5],
		baseDistance - widths[6],
	]
	//This needs to be calculated because distance includes part of button width but margin should not
	const margins = [
		distances[0] - widths[1],
		distances[1] - widths[1],
		distances[2] - widths[2],
		distances[3] - widths[3],
		distances[4] - widths[4],
		distances[5] - widths[5],
		distances[6] - widths[6]
	]
	const [condition, setCurrentCondition] = useState(0)
	const [width, setWidth] = useState(widths[condition])
	const [distance, setDistance] = useState(distances[condition])
	const [margin, setMargin] = useState(margins[condition])
	const [clicks, setClicks] = useState(0)
	const [leftId, setLeftId] = useState("leftButton")
	const [rightId, setRightId] = useState("rightButton")
	const [firstClick, setFirstClick] = useState(true)
	const [finished, setFinished] = useState(false)


	//Wrapping element: anything inside this becomes clickable. When you
	// click it, it logs the event to the console and stores it in the data
	// array.
	const LoggingWrapper = (props) => {
		return (
			<Pressable
				onPress={evt => {
					let x = evt.pageX || evt.nativeEvent.pageX;
					let y = evt.pageY || evt.nativeEvent.pageY;
					let miss = false
					if (props.id != leftId && props.id != rightId) {
						miss = true
					}
					if (firstClick) {
						//Push column names
						console.log("position", "condition", "time", "x", "y", "distance", "width", "miss", "id");
						data.push(["position", "condition", "time", "x", "y", "distance", "width", "miss", "id"]);
						setFirstClick(false)
					}
					console.log(position, condition, Date.now(), x, y, distance, width, miss, props.id);
					data.push([position, condition, Date.now(), x, y, distance, width, miss, props.id]);
					if (clicks == requiredClicks - 1) {
						updateData()
					} else {
						setClicks(clicks + 1)
					}
				}}
			>
				{props.children}
			</Pressable>
		);
	}

	const updateData = (props) => {
		if (condition == 6) {
			setFinished(true)
		} else {
			setWidth(widths[condition + 1])
			setDistance(distances[condition + 1])
			setMargin(margins[condition + 1])
			setClicks(0)
			setCurrentCondition(condition + 1)
		}
	}

	//Simple conversion of the data array to CSV
	const data2csv = (arr) => {
		return arr.map(row => row.join()).join("\n");
	}

	return (
		<View>
			{/* Shown only if !finished */}
			{!finished &&
				<View style={{ flexDirection: 'row' }}>
					<LoggingWrapper id="leftFiller">
						<View
							style={{ width: leftMargin, height: '100%', backgroundColor: 'white' }}
						/>
					</LoggingWrapper>
					<LoggingWrapper id={leftId}>
						<View
							style={{ width: width, height: '100%', backgroundColor: 'steelblue' }}
						/>
					</LoggingWrapper>
					<LoggingWrapper id="middleFiller">
						<View
							style={{ width: margin, height: '100%', backgroundColor: 'white' }}
						/>
					</LoggingWrapper>
					<LoggingWrapper id={rightId}>
						<View
							style={{ width: width, height: '100%', backgroundColor: 'steelblue' }}
						/>
					</LoggingWrapper>
					<LoggingWrapper id="rightFiller">
						<View
							style={{ width: 150, height: '85%', backgroundColor: 'white' }}
						/>
					</LoggingWrapper>

				</View>
			}
			
			{/* Shown only if finished */}
			{ finished &&
				<View style={{marginTop: '100%'}}>
					<Button
						title="Download results"
						onPress={async () => {
							try {
								await Share.share({ message: data2csv(data) });
							} catch (error) {
								alert(error.message);
							}
						}}
					/>
				</View>
			}

		</View>
	);
};

export default Content;


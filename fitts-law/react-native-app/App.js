import React from 'react';
import Content from './components/Content'

//The actual app that gets run
export default function App() {
	return (
		<Content />
	);
};

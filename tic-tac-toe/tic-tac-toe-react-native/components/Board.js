import React from 'react';
import { StyleSheet, View } from 'react-native';
import Square from './Square';

const Board = props => {
  const renderSquare = (i) => {
    return (
      <Square
        value={props.squares[i]}
        onPress={() => props.onPress(i)}
      />
    );
  }

  return (
    <View style={styles.container}>
      <View>
        {renderSquare(0)}
        {renderSquare(1)}
        {renderSquare(2)}
      </View>
      <View>
        {renderSquare(3)}
        {renderSquare(4)}
        {renderSquare(5)}
      </View>
      <View>
        {renderSquare(6)}
        {renderSquare(7)}
        {renderSquare(8)}
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    borderWidth: 1
  },
});

export default Board;
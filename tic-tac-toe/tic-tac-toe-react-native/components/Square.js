import React from 'react';
import { StyleSheet, Text, TouchableOpacity, Dimensions } from 'react-native';

const Square = props => {
  const DeviceWidth = Dimensions.get('window').width;
  const buttonStyle = {
    width: DeviceWidth * 0.2,
    height: DeviceWidth * 0.2,
    borderWidth: 1,
    borderColor: 'black',
    backgroundColor: 'white',
    alignItems: 'center',
    justifyContent: 'center'
  }
  return (
    <TouchableOpacity
      style={buttonStyle}
      onPress={props.onPress}
    >
      <Text style={styles.text}>{props.value}</Text>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  text: {
    fontSize: 34
  },
});

export default Square;
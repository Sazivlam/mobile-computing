import React, { useState } from 'react';
import { StyleSheet, Text, View, TouchableOpacity, ScrollView } from 'react-native';
import Board from './Board';

const Game = props => {
  const [history, setHistory] = useState([{ squares: Array(9).fill(null), }])
  const [stepNumber, setStepNumber] = useState(0)
  const [xIsNext, setXIsNext] = useState(true)

  const handleClick = (i) => {
    const historyCopy = history.slice(0, stepNumber + 1);
    const current = historyCopy[historyCopy.length - 1];
    const squares = current.squares.slice();
    if (calculateWinner(squares) || squares[i]) {
      return;
    }
    squares[i] = xIsNext ? 'X' : 'O';
    setHistory(historyCopy.concat([{
      squares: squares,
    }]));
    setStepNumber(historyCopy.length);
    setXIsNext(!xIsNext)
  }

  const jumpTo = (step) => {
    setStepNumber(step)
    setXIsNext((step % 2) === 0)
  }

  const calculateWinner = (squares) => {
    const lines = [
      [0, 1, 2],
      [3, 4, 5],
      [6, 7, 8],
      [0, 3, 6],
      [1, 4, 7],
      [2, 5, 8],
      [0, 4, 8],
      [2, 4, 6],
    ];
    for (let i = 0; i < lines.length; i++) {
      const [a, b, c] = lines[i];
      if (squares[a] && squares[a] === squares[b] && squares[a] === squares[c]) {
        return squares[a];
      }
    }
    return null;
  }

  const current = history[stepNumber];
  const winner = calculateWinner(current.squares);

  const moves = history.map((step, move) => {
    const desc = move ?
      'Go to move #' + move :
      'Go to game start';
    return (
      <TouchableOpacity
        style={styles.item}
        onPress={() => jumpTo(move)}
      >
        <Text style={styles.textStyle}>{desc}</Text>
      </TouchableOpacity>
    );
  });

  let status;
  if (winner) {
    status = 'Winner: ' + winner;
  } else {
    status = 'Next player: ' + (xIsNext ? 'X' : 'O');
  }

  return (
    <View style={styles.game}>
      <View style={styles.textContainer}>
        <Text style={styles.statusStyle}>
          {status}
        </Text>
      </View>
      <View>
        <Board
          squares={current.squares}
          onPress={(i) => handleClick(i)}
        />
      </View>
      <ScrollView style={styles.buttonList}>
        {moves}
      </ScrollView>

    </View>

  );
};


const styles = StyleSheet.create({
  game: {
    flex: 1,
    marginTop: '20%',
    alignItems: 'center'
  },
  item: {
    borderWidth: 1,
    padding: 5,
    marginBottom: 5,
    marginTop: 5
  },
  statusStyle: {
    fontSize: 24
  },
  textStyle: {
    fontSize: 18
  },
  buttonList: {
    marginBottom: '5%',
    marginTop: '5%'
  },
  textContainer: {
    marginBottom: '2%'
  }
});

export default Game;